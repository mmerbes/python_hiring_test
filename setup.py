import setuptools

setuptools.setup(name='python_hiring_test',
                version='0.1',
                description='Python Hiring Test',
                author='Matt Erbes',
                author_email='mmerbes@gmail.com')
