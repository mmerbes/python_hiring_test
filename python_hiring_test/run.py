"""Main script for generating output.csv."""
import python_hiring_test.pitchdata_parser as parser
import pandas as pan

def main():
    data = pan.read_csv('./data/raw/pitchdata.csv', header=0)
    combos = pan.read_csv('./data/reference/combinations.txt', header=0)
    output = pan.DataFrame(columns=['SubjectId','Stat','Split','Subject','Value'])
    
    output = parser.parsePitchData(data, combos, output)
    output = output.sort_values(['SubjectId','Stat','Split','Subject'],
                                ascending=[1,1,1,1])
    output.to_csv('./data/processed/output.csv', index=False)
    pass


if __name__ == '__main__':
    main()
