import numpy as np
import pandas

splitDict = {
        'vs RHP': ['PitcherSide', 'R'],
        'vs LHP': ['PitcherSide', 'L'],
        'vs RHH' : ['HitterSide', 'R'],
        'vs LHH' : ['HitterSide', 'L']
        }

statOptions = {
        'AVG' : (lambda r: r[1] / r[2]),
        'SLG' : (lambda r: (r[1] + r[3] + (r[4]*2) + (r[5]*3))/r[2]),
        'OBP' : (lambda r: (r[1] + r[6] + r[7])/(r[2] + r[6] + r[7] + r[8])),
        'OPS' : (lambda r: statOptions['SLG'](r) + statOptions['OBP'](r))
        }
#           1   2     3     4     5     6     7      8     9
sortBy = ['H','AB', '2B', '3B', 'HR', 'BB', 'HBP', 'SF', 'PA']

def parsePitchData(data, combos, output):
    for row in combos.itertuples():
        stat, subject, split = row[1], row[2], row[3]
        pd = data.loc[data[splitDict[split][0]]
                            == splitDict[split][1]].groupby(subject)
        pd = pd[sortBy].agg('sum')
        pd = pd.loc[pd['PA'] >= 25]
        results = []
        for r in pd.itertuples():
            results.append(round(statOptions[stat](r),3))
        size = len(pd.index)
        tempOut = pandas.DataFrame(columns=['SubjectId'], data=pd.index.values)
        tempOut = tempOut.assign(Stat=pandas.Series(np.repeat(stat,size)).values)
        tempOut = tempOut.assign(Split=pandas.Series(np.repeat(split,size)).values)
        tempOut = tempOut.assign(Subject=pandas.Series(np.repeat(subject,size)).values)
        tempOut = tempOut.assign(Value=pandas.Series(results).values)
        output = pandas.concat([output,tempOut])
        output['SubjectId'] = output['SubjectId'].apply(np.int64)
    return output
